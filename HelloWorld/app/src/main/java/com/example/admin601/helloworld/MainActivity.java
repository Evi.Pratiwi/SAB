package com.example.admin601.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText nrp, nama;
    Button submit;
    String jurusan = "304";
    String ambil = jurusan.substring(3);
    String batas = jurusan.substring(3,5);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nrp = (EditText) findViewById(R.id.nrp);
        nama = (EditText) findViewById(R.id.nama);
        submit = (Button) findViewById(R.id.btn_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(nrp.getText().equals(batas)){
                    Toast.makeText( MainActivity.this,  "Hello SAB, perkenalkan saya "+nama.getText()
                            +"dengan nrp "+nrp.getText(), Toast.LENGTH_LONG).show();
                }

            }
        });

    }
}
